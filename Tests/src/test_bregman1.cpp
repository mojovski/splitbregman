#include <iostream>
#include <stdio.h>
#include <vector>

#include <Types.h>
#include <SplitBregman.h>

using namespace std;
using namespace os;

// ------------------
// the regression function providing jac and operator ().
// Basically ||Au-y||_2^2
//--------------------
class MyRegression {
public:
	VectorD y;
	MatrixD A;
	MyRegression(VectorD& y, MatrixD& A){
		this->y=y;
		this->A=A;
	}

    double operator()(VectorD& u)const {
        return  (A*u-y).norm();
    }
    MatrixD jac(VectorD& u){
    	return A;
    }

    int targetSize()
    {
    	return A.cols();
    }
};

//---------------------
//	The inner part of the regularization Operator.
// Returns the vector d for |d|, with d=MyPenalty:::(u)
//---------------------
class MyPenalty{
public:
	MatrixD D;
	MyPenalty(MatrixD& D){
		this->D=D;
	}

	VectorD operator()(VectorD& u)
	{
		double res=0;
		VectorD p=this->D*u;
		return p;
	}

	MatrixD jac(VectorD& u)
	{
		return D;
	}

	int outputSize()
	{
		return D.rows();
	}
};

/** Constrcts an easy L1 problem and solves it using the
split bregman method provided by the SplitBregman.h class
**/
//void test_bregman1() 
int main()
{
	/*
	Generate a 	vector x as a known solution.
	*/
	int n=10; //number of variables of interest (dimension of target vector)
	int m=5; //amount of measurements (dimension of y vector)
	VectorD x(n);
	for (int i=0; i<m; i++)
		x[i]=1;
	for (int i=m; i<n; i++)
		x[i]=2;
	
	/*
	Construct the matrix A measuring only
	every 2nd element in x.
	But
	*/
	MatrixD A(m,n);
	A.setZero();
	int step_x=n/m;
	for (int i=0; i<m; i++){
		int ni=i*step_x;
		A(i,ni)=1.0;
	}
	std::cout << "A:\n"<< A;

	//------------------
	//	the measurement y
	//---------------------
	VectorD y(m);
	for (int i=0; i<(m/2);i++)
		y[i]=1;
	for (int i=m/2; i<m; i++)
		y[i]=2;

	std::cout << "\ny: "<< y.transpose();

	//------------------------
	//	the differential operator D
	//------------------------
	MatrixD D(n-1, n);
	D.setZero();
	for (int i=0; i<n-1; i++){
		D(i,i)=1;
		D(i,i+1)=-1;
	}

	std::cout << "\nD: "<< D;


	//------------
	//	obtain the solution
	//------------
	MyRegression my_regression(y, A);
	MyPenalty my_penalty(D);
	double lambda=10.0;
	SplitBregman<MyPenalty, MyRegression> sb;
	SplitBregman<MyPenalty, MyRegression>::Solution sol=sb.solve(my_regression, my_penalty, lambda);

	std::cout << "x_gt: " << x.transpose();
	std::cout <<"\nx_sol: " << sol.x.transpose();

	return 0;
}
