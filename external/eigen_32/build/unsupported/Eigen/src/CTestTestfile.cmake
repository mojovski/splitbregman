# CMake generated Testfile for 
# Source directory: /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/unsupported/Eigen/src
# Build directory: /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/unsupported/Eigen/src
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(AutoDiff)
SUBDIRS(BVH)
SUBDIRS(FFT)
SUBDIRS(IterativeSolvers)
SUBDIRS(MatrixFunctions)
SUBDIRS(MoreVectorization)
SUBDIRS(NonLinearOptimization)
SUBDIRS(NumericalDiff)
SUBDIRS(Polynomials)
SUBDIRS(Skyline)
SUBDIRS(SparseExtra)
SUBDIRS(KroneckerProduct)
SUBDIRS(Splines)
