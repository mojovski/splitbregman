# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build

# Include any dependencies generated for this target.
include unsupported/test/CMakeFiles/NonLinearOptimization.dir/depend.make

# Include the progress variables for this target.
include unsupported/test/CMakeFiles/NonLinearOptimization.dir/progress.make

# Include the compile flags for this target's objects.
include unsupported/test/CMakeFiles/NonLinearOptimization.dir/flags.make

unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o: unsupported/test/CMakeFiles/NonLinearOptimization.dir/flags.make
unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o: ../unsupported/test/NonLinearOptimization.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o"
	cd /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/unsupported/test && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS)  -DEIGEN_TEST_MAX_SIZE=320 -DEIGEN_TEST_FUNC=NonLinearOptimization   -o CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o -c /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/unsupported/test/NonLinearOptimization.cpp

unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.i"
	cd /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/unsupported/test && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS)  -DEIGEN_TEST_MAX_SIZE=320 -DEIGEN_TEST_FUNC=NonLinearOptimization   -E /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/unsupported/test/NonLinearOptimization.cpp > CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.i

unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.s"
	cd /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/unsupported/test && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS)  -DEIGEN_TEST_MAX_SIZE=320 -DEIGEN_TEST_FUNC=NonLinearOptimization   -S /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/unsupported/test/NonLinearOptimization.cpp -o CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.s

unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o.requires:
.PHONY : unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o.requires

unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o.provides: unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o.requires
	$(MAKE) -f unsupported/test/CMakeFiles/NonLinearOptimization.dir/build.make unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o.provides.build
.PHONY : unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o.provides

unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o.provides.build: unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o

# Object files for target NonLinearOptimization
NonLinearOptimization_OBJECTS = \
"CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o"

# External object files for target NonLinearOptimization
NonLinearOptimization_EXTERNAL_OBJECTS =

unsupported/test/NonLinearOptimization: unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o
unsupported/test/NonLinearOptimization: unsupported/test/CMakeFiles/NonLinearOptimization.dir/build.make
unsupported/test/NonLinearOptimization: unsupported/test/CMakeFiles/NonLinearOptimization.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable NonLinearOptimization"
	cd /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/unsupported/test && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/NonLinearOptimization.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
unsupported/test/CMakeFiles/NonLinearOptimization.dir/build: unsupported/test/NonLinearOptimization
.PHONY : unsupported/test/CMakeFiles/NonLinearOptimization.dir/build

unsupported/test/CMakeFiles/NonLinearOptimization.dir/requires: unsupported/test/CMakeFiles/NonLinearOptimization.dir/NonLinearOptimization.cpp.o.requires
.PHONY : unsupported/test/CMakeFiles/NonLinearOptimization.dir/requires

unsupported/test/CMakeFiles/NonLinearOptimization.dir/clean:
	cd /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/unsupported/test && $(CMAKE_COMMAND) -P CMakeFiles/NonLinearOptimization.dir/cmake_clean.cmake
.PHONY : unsupported/test/CMakeFiles/NonLinearOptimization.dir/clean

unsupported/test/CMakeFiles/NonLinearOptimization.dir/depend:
	cd /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32 /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/unsupported/test /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/unsupported/test /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/unsupported/test/CMakeFiles/NonLinearOptimization.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : unsupported/test/CMakeFiles/NonLinearOptimization.dir/depend

