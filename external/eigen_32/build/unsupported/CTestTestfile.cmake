# CMake generated Testfile for 
# Source directory: /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/unsupported
# Build directory: /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/unsupported
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(Eigen)
SUBDIRS(doc)
SUBDIRS(test)
