# CMake generated Testfile for 
# Source directory: /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing
# Build directory: /home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/build/blas/testing
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(sblat1 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "sblat1" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/sblat1.dat")
ADD_TEST(sblat2 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "sblat2" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/sblat2.dat")
ADD_TEST(sblat3 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "sblat3" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/sblat3.dat")
ADD_TEST(dblat1 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "dblat1" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/dblat1.dat")
ADD_TEST(dblat2 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "dblat2" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/dblat2.dat")
ADD_TEST(dblat3 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "dblat3" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/dblat3.dat")
ADD_TEST(cblat1 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "cblat1" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/cblat1.dat")
ADD_TEST(cblat2 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "cblat2" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/cblat2.dat")
ADD_TEST(cblat3 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "cblat3" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/cblat3.dat")
ADD_TEST(zblat1 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "zblat1" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/zblat1.dat")
ADD_TEST(zblat2 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "zblat2" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/zblat2.dat")
ADD_TEST(zblat3 "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/runblastest.sh" "zblat3" "/home/eugen/projects/cpp/libs/SplitBregman/external/eigen_32/blas/testing/zblat3.dat")
