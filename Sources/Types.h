/*
 * Types.hpp
 *
 *  Created on: Oct 22, 2013
 *      Author: Eugen
 */

#ifndef TYPES_HPP_
#define TYPES_HPP_

#include <deque>
#include <cmath>
#include <deque>
#include <Eigen/Core>

namespace os {

typedef Eigen::Matrix<double, 3, 1> Point3D;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorD;
typedef Eigen::Matrix<float, Eigen::Dynamic, 1> VectorF;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixD;
typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> MatrixF;


}

#endif /* TYPES_HPP_ */
