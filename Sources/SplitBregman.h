/*
 * SplitBregman.h
 *
 *  Created on: October 22, 2013
 *      Author: eugen
 *      Implements the SPlitBregman method based on the paper
 */

#ifndef SPLITBREGMAN_H_
#define SPLITBREGMAN_H_

#include <Types.h>
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/Cholesky>
#include <Eigen/LU>
#include <Eigen/OrderingMethods>
#include <Eigen/SparseCholesky>

namespace os {
	double max(double a, double b)
	{
		if (a>b){
			return a;
		} else {
			return b;
		}
	}

	double dabs(double a)
	{
		if (a>(-a))
		{
			return a;
		} else 
		{
			return -a;
		}
	}
	/**
	THe Split Bregman method solves
	
		min |\Phi(u)| + H(u)

	where the penalizing operator \Phi is the Penfcn (first template argument)
	and H(u) is the regression function (second template argument).
	The template functions should provide the () operator, the grad() method, and
	sometimes the hessian method hess().
	**/
	template<typename PenFcn, typename RegFcn>
	class SplitBregman{
	public:
		class Solution {
		public:
			Solution(){};
			VectorD x;
			bool success;
		};
		SplitBregman(){};

		/**
		Basically solving the problem of type:
		min |d| + H(u)
		s.t. d=\Phi(u)
		According to eq 3.2, it is transformed to
		min |d| + H(u)+ \lambda/2 || d- \Phi(u) ||_2^2

		The iterative optimization runs over two levels:
		1. Level: THe SPlit Bregman Iteration
			1.1 (u[k+1], d[k+1])= min(...)
			1.2 b[k+1]=...
		The second level is the solution for 1.1.
		2:
			2.1 u_(k+1)=min_u H(u)+\lambda/2 ||d_(k)-\Phi(u)-b_(k)||_2^2
			2.2 d_(k+1)=min_d |d| \lambda/2 ||d-\Phi(u_(k+1))-b_(k)||_2^2
		
		So obtaining the solution of 2.1 can be achieved using common optimization methods.

		The solve method works basically like this:
		while ||u[k]-u[k+1]||<tol
			for n:N //N is the number of max iterations
				u[k+1]=solve_2.1
				d[k+1]=solve_2.2
			end
			b[k+1]=b[k]+Phi(u[k+1])-d[k+1]
		end
		@param b is the regression target from ||Au-b||
		**/
		Solution solve(RegFcn & H, PenFcn & P, double lambda, int N=1, double thr=1e-3){
			VectorD u(H.targetSize()), u_k(H.targetSize()), d(P.outputSize()),b;
			u.setZero();
			d.setZero();//b and d must have same dimensions, since b is the slack variabel for d
			b=d;
			double update_size=1e4;
			u_k=u;
			while (update_size>thr)
			{
				for (int i=0; i<N; i++)
				{
					u=solve_21(u, d, b, H, P); //liefert nahe der Lösung NaNs!!!
					d=solve_22(u, d, b, H, P, lambda);
				}
				b=b+P(u)-d;
				update_size=(u-u_k).norm();
				u_k=u;
			}
			Solution s;
			s.x=u;
			return s;
		}

		/**
		Solves the step 2.1 as described above.
		Assume that H(u)=||y-A*U||_2^2 and
		Phi(u)=P*u
		the problem can be restated as 
			min_u (-2d^T P + b^T*P -2y^T*A)u -u^T(P^TP+ A^T+A)u
		which is basically the an unconstrained problem, of which the solution is
		x=-(P'^T+P')^{-1}c
		Now, as P'=(P^TP+ A^T+A), P'^T=(PP^T+AA^T). This is used for the LLT (LDLT) Cholesky factorization

		**/
		VectorD solve_21(VectorD &u, VectorD& d, VectorD& b, RegFcn & Hf, PenFcn & Pf){
			//-------------
			//	1. COnstruct the Matrix (P'^T+P')
			//-------------
			MatrixD P=Pf.jac(u);
			MatrixD A=Hf.jac(u);
			MatrixD P1=P.transpose()*P; //P*P.transpose();
			MatrixD P2=A.transpose()*A; //A*A.transpose();

			MatrixD Ps=P1+P2; //P*P.transpose()+A*A.transpose(); //!! CHECK this according to the Matrix Cookbook

			//------------
			//	1. Construct the vector c
			//------------
			std::cout << "\nd: "<< d.transpose();
			std::cout << "\nb: " << b.transpose();
			std::cout << "\nb-d: \n" << b.transpose()-d.transpose();
			VectorD c=2*(b.transpose()-d.transpose())*P-2*Hf.y.transpose()*A;

			//--------------
			//	3. Solve for u
			//--------------
			//Eigen::SparseQR<Eigen::SparseMatrix<T>,  Eigen::COLAMDOrdering<int> > solver(A);
			std::cout << "\nPs+Ps.T: \n" << Ps+Ps.transpose();
			VectorD res=(Ps+Ps.transpose()).llt().solve(-c);
			std::cout << "\nres: " << res.transpose();
			return res;
		}

		/**
		The solution for d[k+1] is obtained according to the formular on page 8 of the paper
		Writes the result into the passed vector d.
		**/
		VectorD solve_22(VectorD &u, VectorD& d, VectorD& b, RegFcn & H, PenFcn & P, double lambda)
		{
			VectorD Pu=P(u);
			for (int i=0; i<d.size(); i++)
			{
				std::cout << "\ndi: " << d[i];
				d[i]=shrink(Pu[i]+b[i], 1/lambda);
				std::cout << "\tdi: " << d[i];
			}
			return d;

		}
		inline double shrink(double x, double gamma)
		{
			std::cout << "\t shrink x: " << x << ", gamma: " << gamma;
			if (dabs(x)<1e-3)
			{
				return 0;
			} else
				return x/(dabs(x))*max(dabs(x)-gamma, 0);
		}

		/**
		Writes the output into b
		**/
		inline void solve_12(PenFcn& P, VectorD & u, VectorD& d, VectorD& b)
		{
			b=b+P(u)-d;
		}

		/**
		This solves the step 1.1, which basically iterates thorugh the steps 2.1 and 2.2.
		**/
		void solve_11()
		{

		}
		
	};

};


#endif /* SPLITBREGMAN_H_ */
