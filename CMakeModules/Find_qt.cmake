FIND_PACKAGE(Qt4 REQUIRED)   		# note that it's Qt4, not QT4 or qt4

# Defines the following libraries:
#	${QT_LIBRARIES}
#	${QT_QTCORE_LIBRARY} 
#	${QT_QTGUI_LIBRARY} 
#	${QT_QTMAIN_LIBRARY}
#	${QT_QTOPENGL_LIBRARY}
	
IF(NOT QT4_FOUND)
     MESSAGE("** Error finding QT4 library: ")
     MESSAGE(FATAL_ERROR "** MFC-DAPS/${PROJECTNAME} requires QT4.")
ENDIF()

SET( QT_USE_QT3SUPPORT TRUE )
SET( QT_USE_QTXML TRUE )
SET( QT_USE_QTOPENGL TRUE )

MESSAGE(STATUS " USING THE QT FILE::: (${QT_USE_FILE})")
IF(WIN32)
	MESSAGE(STATUS "---------------WIN32-----------")
	#string(REPLACE "/" "\" qt_use_file_normal "${QT_USE_FILE}") 
	#string(REPLACE " " "\ " qt_use_file_normal "${qt_use_file_normal}") 
	SET(qt_use_file_normal ${QT_USE_FILE})
ELSEIF(WIN32)
	MESSAGE(STATUS "--------NOT-------WIN32-----------")
	SET(qt_use_file_normal ${QT_USE_FILE})
ENDIF(WIN32)
MESSAGE(STATUS "NEW QT USE FILE: ${qt_use_file_normal}")
INCLUDE(${qt_use_file_normal})


INCLUDE_DIRECTORIES(
	${CMAKE_CURRENT_BINARY_DIR}
	${QT_INCLUDE_DIR}
	${QT_QTOPENGL_INCLUDE_DIR}
)

LINK_DIRECTORIES(
	${QT_LIBRARY_DIR}
	${QT_QTOPENGL_INCLUDE_DIR}
)

############## QT Build rules #######################################################
# this command will generate rules that will run rcc on all files from RCS
# in result RC_SRCS variable will contain paths to files produced by rcc
QT4_ADD_RESOURCES( RC_SRCS ${RCS} )

# this will run uic on .ui files:
QT4_WRAP_UI( UI_HDRS ${UIS} )

# and finally this will run moc:
QT4_WRAP_CPP( MOC_SRCS ${MOC_HDRS} )