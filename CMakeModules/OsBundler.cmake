MACRO(CMAKE_PARSE_ARGUMENTS prefix arg_names option_names)
  SET(DEFAULT_ARGS)
  FOREACH(arg_name ${arg_names})
    SET(${prefix}_${arg_name})
  ENDFOREACH(arg_name)
  FOREACH(option ${option_names})
    SET(${prefix}_${option} FALSE)
  ENDFOREACH(option)

  SET(current_arg_name DEFAULT_ARGS)
  SET(current_arg_list)
  FOREACH(arg ${ARGN})
    LIST_CONTAINS(is_arg_name ${arg} ${arg_names})
    IF (is_arg_name)
      SET(${prefix}_${current_arg_name} ${current_arg_list})
      SET(current_arg_name ${arg})
      SET(current_arg_list)
    ELSE (is_arg_name)
      LIST_CONTAINS(is_option ${arg} ${option_names})
      IF (is_option)
	SET(${prefix}_${arg} TRUE)
      ELSE (is_option)
	SET(current_arg_list ${current_arg_list} ${arg})
      ENDIF (is_option)
    ENDIF (is_arg_name)
  ENDFOREACH(arg)
  SET(${prefix}_${current_arg_name} ${current_arg_list})
ENDMACRO(CMAKE_PARSE_ARGUMENTS)
#


MACRO (REGISTER_GLOBAL_LIB NAME)
	#MESSAGE(STATUS ">> Registering a global lib: ${NAME}")
	GET_PROPERTY(LIBS_STORE
		GLOBAL
		PROPERTY GLOBAL_LIBS
		)
		
	SET_PROPERTY(GLOBAL 
		PROPERTY GLOBAL_LIBS
		${LIBS_STORE}
		${NAME}
		)
ENDMACRO(REGISTER_GLOBAL_LIB)

	
MACRO (OsIncludePackage NAME)

	#construct the path to the bundler cmake modules
	SET(osbundler_path "$ENV{OSBUNDLERDIR}")
	#name_lowercase
	string(TOLOWER ${NAME} name_lowercase)
	#MESSAGE(STATUS "NAME: ${NAME}, name_lowercase: ${name_lowercase}, calling: Find_${name_lowercase}.cmake ")
	set(options OPTIONAL)
	CMAKE_PARSE_ARGUMENTS("INCLUDING" "${options}" "" "" ${ARGN} )
	MESSAGE(STATUS "options OsIncludePackage(${NAME}) is OPTIONAL: ${INCLUDING_OPTIONAL}")
	include("${CMAKE_SOURCE_DIR}/CMakeModules/Find_${name_lowercase}.cmake")

	
	string(LENGTH "${${NAME}_LIBRARIES}" LL)
	IF (LL GREATER 0)
		#MESSAGE(STATUS "REGISTERING GLOBAL LIB ${${NAME}_LIBRARIES}")
		REGISTER_GLOBAL_LIB("${${NAME}_LIBRARIES}")
	ENDIF(LL GREATER 0)

ENDMACRO(OsIncludePackage)



