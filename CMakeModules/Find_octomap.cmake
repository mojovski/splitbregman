include("${PROJECT_SOURCE_DIR}/external/octomap-1.6.1/octomap/lib/cmake/octomap/octomap-config.cmake")
#sets OCTOMAP_LIBRARY_DIRS, OCTOMAP_INCLUDE_DIRS and OCTOMAP_LIBRARIES
include_directories(${OCTOMAP_INCLUDE_DIRS})
link_directories(${OCTOMAP_LIBRARY_DIRS})
REGISTER_GLOBAL_LIB("${OCTOMAP_LIBRARIES}")
MESSAGE(STATUS "OCTOMAP LIBS: ${OCTOMAP_LIBRARIES}")
