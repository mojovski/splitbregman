#--------------------
#	include VisLib3D
#--------------------
#the vislib3d is expected in the same directory as the oslib2
SET(VISLIB3D_DIR "$ENV{OSLib2_DIR}/../VisLib3D/")
#MESSAGE(STATUS "VisLib3D set to : ${VISLIB3D_DIR}")
include("${VISLIB3D_DIR}Config.cmake")

#MESSAGE(STATUS "INCLUDING VISLIB DIRS:${VisLib3D_INCLUDE_DIRS}")
INCLUDE_DIRECTORIES(${VisLib3D_INCLUDE_DIRS})
LINK_DIRECTORIES(${VisLib3D_LIBRARY_DIRS})

REGISTER_GLOBAL_LIB("${VisLib3D_LIBRARIES}")
#MESSAGE(STATUS "VISLIB REGISTER:${VisLib3D_LIBRARIES}")

#target_link_libraries(${PROJECTNAME}
#	${OSLIB_LIBRARIES}
#	${VisLib3D_Libraries}
#)

#you can also include vislib3d to your project:
#INCLUDE_EXTERNAL_MSPROJECT(VisLib3D ${VISLIB3D_DIR}/VisLib3D.vcproj)
