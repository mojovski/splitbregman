#locate_os_package(KalmanFilterLib)
#find_package(KalmanFilterLib REQUIRED)
#locate_os_package(TrackerLib)
#find_package(TrackerLib REQUIRED)

set(TrackerFeeder_DIR "$ENV{OSLib2_DIR}/../TrackerFeederPlugin")
locate_os_package(TrackerFeeder)
find_package(TrackerFeeder REQUIRED)

REGISTER_GLOBAL_LIB("${TRACKERFEEDER_LIBRARIES}")

#MESSAGE(STATUS "\n----------------  INCLUDE TRACKER FEEDER DIR  -----------------\n ${TRACKERFEEDER_INCLUDE_DIRS}")
include_directories (
	${TRACKERFEEDER_INCLUDE_DIRS}
)

LINK_DIRECTORIES(
	${TRACKERFEEDER_LIBRARY_DIRS}
)